package com.szkolenieandroid.animationsactivity;

import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Random;


public class AnimationsActivity2 extends Activity {

    TextView firstView;
    RelativeLayout rootContainer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animations);
        initViews();
    }

    private void initViews() {
        firstView = (TextView) findViewById(R.id.textView);
        rootContainer = (RelativeLayout) findViewById(R.id.rootContainer);
        dragAnimator = new DragAnimator();
        dragAnimator.setDraggedView(firstView);
    }

    DragAnimator dragAnimator;
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.animations, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = new Intent(this, SecondActivity.class);
        ActivityOptions options = ActivityOptions.makeScaleUpAnimation(firstView, 0,
                0, firstView.getWidth(), firstView.getHeight());

        startActivity(intent, options.toBundle());
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void runAnimation(View view) {
//        this.createAnimation().start();
//        view.animate().translationX(200).alpha(0.5f).withLayer().setDuration(1000);
//        colorAnimationChange();


//        textView.animate().alpha(0).setDuration(1000).withEndAction(new Runnable() {
//            public void run() {
//                // Remove the view from the layout called parent
//                rootContainer.removeView(textView);
//            }
//        });

//        rotateView();
        paintView();
    }

    private void colorAnimationChange() {
        Integer colorFrom = getResources().getColor(android.R.color.holo_red_dark);
        Integer colorTo = getResources().getColor(android.R.color.holo_green_dark);
        ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo);
        colorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                firstView.setBackgroundColor((Integer) animator.getAnimatedValue());
            }

        });
        colorAnimation.setDuration(1000);
        colorAnimation.start();
    }

    private void rotateView() {
        float dest = 360;
        if (firstView.getRotation() == 360) {
            System.out.println(firstView.getAlpha());
            dest = 0;
        }
        ObjectAnimator animation1 = ObjectAnimator.ofFloat(firstView,
                "rotation", dest);
        animation1.setDuration(2000);
        animation1.start();
    }
    private void paintView() {
        float dest = 0;
        Paint paint = new Paint();
        TextView aniTextView = (TextView) findViewById(R.id.textView);
        float measureTextCenter = paint.measureText(aniTextView.getText()
                .toString());

        int textSize = 120;
        float curTextSize = 0;
        for(int i = 2; i<18 && curTextSize< textSize;i+=2)
        {
            aniTextView.setTextSize(i);
            curTextSize = aniTextView.getPaint().measureText(aniTextView.getText().toString());
        }


        measureTextCenter +=60;
        dest = 0 - measureTextCenter;
        if (aniTextView.getX() < 0) {
            dest = 0;
        }
        ObjectAnimator animation2 = ObjectAnimator.ofFloat(aniTextView,
                "x", dest);
        animation2.setDuration(2000);
        animation2.start();
    }

    private AnimatorSet createAnimation() {
        int width = getWindowManager().getDefaultDisplay().getWidth();
        int height = getWindowManager().getDefaultDisplay().getHeight();

        Random randon = new Random();
        int nextX = randon.nextInt(width);
        int nextY = randon.nextInt(height);
        Button button = (Button) findViewById(R.id.button_run);
        ObjectAnimator animation1 = ObjectAnimator.ofFloat(button, "translationX", nextX);
        animation1.setDuration(1400);
        ObjectAnimator animation2 = ObjectAnimator.ofFloat(button, "translationy", nextY);
        animation2.setDuration(1400);
        AnimatorSet set = new AnimatorSet();
        set.playTogether(animation1, animation2);
        set.start();
        return set;
    }
}
