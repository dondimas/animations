package com.szkolenieandroid.animationsactivity;

import android.animation.ObjectAnimator;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

/**
 * Created by dmitry on 18/11/14.
 */
public class DragAnimator implements View.OnDragListener, View.OnTouchListener {
    View draggedView;
    public void setDraggedView(TextView firstView) {
        draggedView = firstView;
        draggedView.setOnDragListener(this);
    }

    @Override
    public boolean onDrag(View v, DragEvent event) {
        moveToX(event.getX());
        return false;
    }

    private void moveToX(float x) {
        ObjectAnimator animation2 = ObjectAnimator.ofFloat(draggedView,
                "x", x);
        animation2.setDuration(500);
        animation2.start();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        moveToX(event.getX());
        return false;
    }
}
