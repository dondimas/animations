package com.szkolenieandroid.animationsactivity;

import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.AnticipateInterpolator;
import android.view.animation.BounceInterpolator;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Random;


public class AnimationsActivity extends Activity {

    TextView textView;
    RelativeLayout rootContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animations);
        initViews();
    }

    private void initViews() {
        textView = (TextView) findViewById(R.id.textView);
        rootContainer = (RelativeLayout) findViewById(R.id.rootContainer);
    }




    public void runAnimation(View view) {
//        colorAnimationChange();
//        rotateView();
//        moveX(textView);
//        animateWitListener(textView);
//        createAnimation();
//
//        changeVisibilityLayoutAnimation(textView);
//        loadXmlSoom(textView);
        startActivity(new Intent(this, SecondActivity.class));
    }

    private void loadXmlSoom(View animatedView) {
//        Animation animation = AnimationUtils.loadAnimation(this, R.anim.my_animation);
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.rotare_clockwize);
        animatedView.startAnimation(animation);
    }


    private void changeVisibilityLayoutAnimation(View animatedView) {
        animatedView.setVisibility(animatedView.getVisibility() == View.VISIBLE?View.GONE:View.VISIBLE);
    }

    private void colorAnimationChange(View animatedView) {
        Integer colorFrom = getResources().getColor(android.R.color.holo_red_dark);
        Integer colorTo = getResources().getColor(android.R.color.holo_green_dark);

        ObjectAnimator anim = ObjectAnimator.ofObject(animatedView,
                "textColor",
                new ArgbEvaluator(),
                colorFrom, colorTo);

        anim.setDuration(2000);
        anim.start();

    }

    private void rotateView() {
        float dest = 360;
        if (textView.getRotation() == 360) {
            dest = 0;
        }
        ObjectAnimator animation1 = ObjectAnimator.ofFloat(textView,
                "rotation", dest);
        animation1.setDuration(2000);
        animation1.setInterpolator(new BounceInterpolator());
        animation1.setInterpolator(new AnticipateInterpolator());
        animation1.start();
    }

    private void moveX(View animatedView) {

        int dest =  350;
        if(animatedView.getX() == dest) {
            dest = -dest;
        }
        ObjectAnimator animation = ObjectAnimator.ofFloat(animatedView,
                "x", dest);
        animation.setDuration(getResources().getInteger(android.R.integer.config_longAnimTime));
        animation.setDuration(2000);
        animation.setInterpolator(new AnticipateInterpolator());
        animation.start();
    }

    private void animateWitListener(final View animatedView) {

//        ViewGroup viewParent = (ViewGroup) animatedView.getParent();
//        viewParent.removeView(animatedView);

    animatedView.animate().alpha(0).setDuration(1000).withEndAction(new Runnable() {
            public void run() {
                // Remove the view from the layout called parent
                ViewGroup viewParent = (ViewGroup) animatedView.getParent();
                viewParent.removeView(animatedView);
            }
        });
    }

    private AnimatorSet createAnimation() {
        View animatedView = findViewById(R.id.button_run);
//        String param1 = "rotationX";
//        String param2 =  "rotationY";
        String param1 = "translationX";
        String param2 =  "translationY";
        return createAnimation(textView, param1, param2);
//        return createAnimation(animatedView, param1, param2);
    }
    private AnimatorSet createAnimation(View animatedView,String param1, String param2) {
        int width = getWindowManager().getDefaultDisplay().getWidth();
        int height = getWindowManager().getDefaultDisplay().getHeight();

        Random randon = new Random();
        int nextX = randon.nextInt(width);
        int nextY = randon.nextInt(height);
        nextY*=-1;

        ObjectAnimator animation1 = ObjectAnimator.ofFloat(animatedView, "translationX", nextX);
        animation1.setDuration(1400);
        ObjectAnimator animation2 = ObjectAnimator.ofFloat(animatedView, "rotationY", nextY);
        animation2.setDuration(1400);
        animation1.setInterpolator(new BounceInterpolator());
        AnimatorSet set = new AnimatorSet();
        set.playTogether(animation1, animation2);
        set.start();
        return set;
    }

}
